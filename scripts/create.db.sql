 CONNECT 'jdbc:derby:atlassiandb;create=true';

create table account (
 	ID INT PRIMARY KEY,
    
    companyName VARCHAR(40),
	address1 VARCHAR(40),
	address2 VARCHAR(40),
	city VARCHAR(40),
	state VARCHAR(3),
	postalCode VARCHAR(40),
	country VARCHAR(3)
    );

create table contacts (
 	ID INT PRIMARY KEY,
    
    NAME VARCHAR(40),
	address1 VARCHAR(40),
	address2 VARCHAR(40),
	city VARCHAR(40),
	state VARCHAR(3),
	postalCode VARCHAR(40),
	country VARCHAR(3),
	
	accountId int
    
);

alter table contacts add foreign key (accountid) references account (id);

create index account_idx on contacts ( accountId );
