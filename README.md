( you will have to either 'source' the scripts or chmod +x them )

First, from the project root, create the Derby embedded database with this 
	script , which assumes JAVA_HOME is set, and that the JDK is installed. 

	scripts/create.db.sh

After the script is done, run:

	mvn sprint-boot:run

The server will be started, running on port 8080 .

To test the end points, scripts that repeatedly call curl are included:

	cd scripts/curl
	accounts-post.sh
	contacts-post.sh

The server will log to the console what is received. 

To get the contacts under each account :

	accounts-contacts.sh 


While the application is not running, you can view the db with the IJ command in $JAVA_HOME/db/bin. ( derby is embedded - only one connection at a time, I guess).

Scripts that use IJ:

	scripts/list-tables.sh
	scripts/clear-tables.sh

Still Left todo:

	The API could be more robust :
		If an account is passed to the POST/PUT of /contact that doesn't exist, a 400 should be returned.

		For an GET for account or contact id that doesn't exist , should return 404. 


	The database could grow very large - so indexes on all fields that might be joined on or queried would be important.

	Duplicate POSTS for an id should return a 400.

	A more detailed service layer - it us currently stubbed with JAVADOCS about what each SAVE/UPDATE/GET would do. But it coudl be added between the
		controllers and DAOs.

	More professional logging than System.out.print
	
	Tests! 

	Delete actions could be added.
