package atlassian.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Repository;

import atlassian.domain.Contact;

@Repository
public class ContactDAO {
	
	@Autowired
	SimpleDriverDataSource dataSource;
	
	public List<Contact> getForAccount ( int  accountId )  {
		
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
       
    	String sqlSelect = 
    			"select id, name, address1, address2, city, state, postalcode, country, accountid" +
    			" from Contacts  where accountId = ? ";
    	
        List<Contact> listContact = jdbcTemplate.query (sqlSelect, new Object[] { accountId }, 
        		new BeanPropertyRowMapper(Contact.class) ); 

 		
        System.out.println("size of list : " + listContact.size() );
        
		return listContact;
	}
	
	public Contact get ( int  contactId )  {
		
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
      
    	String sqlSelect = "select id, name, address1, address2, city, state, postalcode, country, accountid" +
    	" from Contacts where id = ? ";
    	
        List<Contact> listContact = jdbcTemplate.query(sqlSelect, new Object[] { contactId }, new RowMapper<Contact>() {
 
            public Contact mapRow(ResultSet result, int rowNum) throws SQLException {
            	Contact contact = new Contact();
                contact.setName(result.getString("name"));
                contact.setId(result.getInt("id"));
                contact.setAddress1(result.getString("address1"));
                contact.setAddress2(result.getString("address2"));
                contact.setCity(result.getString("city"));
                contact.setState(result.getString("state"));
                contact.setPostalCode(result.getString("postalcode"));
                contact.setCountry(result.getString("country"));
                contact.setAccountId(result.getInt("accountid"));
                 
                return contact;
            }
             
        });
 		
		return listContact.get(0);
	}
	
	public int insert(Contact acct) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sqlInsert = "insert into Contacts "
				+ "( id, name, address1, address2, city, state, postalcode, country, accountid ) "
				+ " values ( ?, ? , ? , ? , ?, ? , ? , ?, ? ) ";

		int numRows=jdbcTemplate.update(sqlInsert, 
				acct.getId(),
				acct.getName(),
				acct.getAddress1(),

				acct.getAddress2(),
				acct.getCity(),
				acct.getState(),

				acct.getPostalCode(),
				acct.getCountry(),
				acct.getAccountId()
				);

		return numRows;
	}

	public int update(Contact contact) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sqlUpdate = "update Contacts "
				+ "set name = ?, address1 = ?, address2 = ?, city = ?, state = ?, postalcode = ?, country = ? , accountid= ? ) "
				+ " where id = ? " ;

		int numRows=jdbcTemplate.update(sqlUpdate, 
				contact.getName(),
				contact.getAddress1(),
				contact.getAddress2(),
				contact.getCity(),
				contact.getState(),
				contact.getPostalCode(),
				contact.getCountry(),
				contact.getAccountId(),
				contact.getId()
				);

		return numRows;
	}
}

