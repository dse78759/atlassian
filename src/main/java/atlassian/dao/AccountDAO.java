package atlassian.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Repository;

import atlassian.domain.Account;

@Repository
public class AccountDAO {
	
	@Autowired
	SimpleDriverDataSource dataSource;
	
	public Account get ( int  acctId )  {
		
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
      
    	String sqlSelect = "select id, companyname, address1, address2, city, state, postalcode, country" +
    	" from account ";
    	
        List<Account> listContact = jdbcTemplate.query(sqlSelect, new RowMapper<Account>() {
 
            public Account mapRow(ResultSet result, int rowNum) throws SQLException {
            	Account contact = new Account();
                contact.setCompanyName(result.getString("companyname"));
                contact.setId(result.getInt("id"));
                contact.setAddress1(result.getString("address1"));
                contact.setAddress2(result.getString("address2"));
                contact.setCity(result.getString("city"));
                contact.setState(result.getString("state"));
                contact.setPostalCode(result.getString("postalcode"));
                contact.setCountry(result.getString("country"));
                 
                return contact;
            }
             
        });
 		
		return listContact.get(0);
	}
	
	public int insert(Account acct) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sqlInsert = "insert into account "
				+ "( id, companyname, address1, address2, city, state, postalcode, country ) "
				+ " values ( ?, ? , ? , ? , ?, ? , ? , ? ) ";

		int numRows=jdbcTemplate.update(sqlInsert, 
				acct.getId(),
				acct.getCompanyName(),
				acct.getAddress1(),
				acct.getAddress2(),
				acct.getCity(),
				acct.getState(),
				acct.getPostalCode(),
				acct.getCountry()
				);

		return numRows;
	}

	public int update(Account acct) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sqlUpdate = "update account "
				+ "set companyname = ?, address1 = ?, address2 = ?, city = ?, state = ?, postalcode = ?, country = ? ) "
				+ " where id = ? " ;

		int numRows=jdbcTemplate.update(sqlUpdate, 
				acct.getCompanyName(),
				acct.getAddress1(),
				acct.getAddress2(),
				acct.getCity(),
				acct.getState(),
				acct.getPostalCode(),
				acct.getCountry(),
				acct.getId()
				);

		return numRows;
	}
}

