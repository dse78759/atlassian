package atlassian.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import atlassian.dao.AccountDAO;
import atlassian.dao.ContactDAO;
import atlassian.domain.Account;
import atlassian.domain.Contact;

@RestController 
public class AccountController {

	@Autowired
	AccountDAO accountDao;
	
	@Autowired
	ContactDAO contactDao;
	
	@RequestMapping(value="/account", method=RequestMethod.POST, consumes = "application/json" )
	String createAccount(@RequestBody Account acct) {
		
		System.out.println ( "create account : " + acct );
		
		accountDao.insert(acct);
		
		return "new account made:" + acct.getCompanyName() ;
	}
	
	@RequestMapping(value = "/account", method = RequestMethod.PUT, consumes = "application/json")
	String put(@RequestBody Account acct) {
		System.out.println("updated account : " + acct);

		accountDao.update(acct);

		return "Hello from put: " + acct.getCompanyName();
	}

	@RequestMapping(value = "/account/{id}", method = RequestMethod.GET)
	public ResponseEntity<Account> get (@PathVariable("id") int id)
	    {
		System.out.printf("\ngot getAccountById : %d \n", id);
		
        Account employee = accountDao.get(id);
		
       	return new ResponseEntity<Account>(employee, HttpStatus.OK);
    }

	@RequestMapping(value = "/account/{id}/contacts", method = RequestMethod.GET)
	public ResponseEntity<List<Contact>> getContacts (@PathVariable("id") int id)
	    {
		System.out.printf("\ngot getContacts for Account : %d \n", id);
		
		List<Contact> contacts = contactDao.getForAccount(id);
		
       	return new ResponseEntity<List<Contact>>(contacts, HttpStatus.OK);
    }
}