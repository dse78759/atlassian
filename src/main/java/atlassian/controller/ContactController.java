package atlassian.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import atlassian.dao.ContactDAO;
import atlassian.domain.Contact;

@RestController
public class ContactController {

	@Autowired
	ContactDAO contactDAO;
	
	@RequestMapping(value = "/contact", method = RequestMethod.POST, consumes = "application/json")
	String createContact(@RequestBody Contact contact) {

		System.out.println("create contact : " + contact);
		int rows = contactDAO.insert(contact);
		
		return "num new contacts made : " + rows;
	}

	@RequestMapping(value = "/contact", method = RequestMethod.PUT, consumes = "application/json")
	String put(@RequestBody Contact contact) {
		System.out.println("update contact : " + contact);

		int rows = contactDAO.update(contact);
		
		return "contact updated: " + rows;
	}

	@RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
	public ResponseEntity<Contact> get (@PathVariable("id") int id) {
		System.out.printf("getContactById : %d\n", id);

		Contact contact = contactDAO.get(id);
		
		return new ResponseEntity<Contact>(contact, HttpStatus.OK);

	}

}