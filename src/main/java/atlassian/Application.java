package atlassian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@SpringBootApplication // same as @Configuration @EnableAutoConfiguration @ComponentScan
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 *  For the quick implementation, we'll have the application make the datasource,
	 *  but in a real app, we'd have the container manage the username/password and 
	 *  connection pool. 
	 * @return
	 */
	@Bean 
	SimpleDriverDataSource createDataSource() {
		  SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
	        dataSource.setDriver(new org.apache.derby.jdbc.EmbeddedDriver());
	        dataSource.setUrl("jdbc:derby:atlassiandb");
	        return dataSource;
	}
}
