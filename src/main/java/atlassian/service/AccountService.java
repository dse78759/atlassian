package atlassian.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atlassian.dao.AccountDAO;
import atlassian.domain.Account;

/**
 * UNIMPLEMENTED:
 * 
 * Encapsulate any business logic in this layer, so it can be easily tested...
 * 
 * @author scott
 *
 */
@Service
public class AccountService {

	@Autowired
	AccountDAO accountDao;

	/**
	 * TODO: the logic for save should be:
	 * 		Check if the Account exists already
	 * 		If so, direct the controller layer to send back a 400.
	 * 
	 * 		After save, the post should send back the new id of the record.		
	 * 
	 */
	int save ( Account c ) {
		
		return 0;
	}

	/**
	 * TODO: the logic for update should be:
	 * 		Check if the Account exists already
	 * 		If not, direct the controller layer to send back a 404 not found.
	 * 		
	 */
	int update ( Account c ) {
		
		return 0;
	}

	/** TODO: 
	 * 		If the requested Account is not present, controller should send back a 404.
	 * 
	 * @param id
	 * @return
	 */
	Account get ( int id ) {
		
		return null;
	}
	
	
}
