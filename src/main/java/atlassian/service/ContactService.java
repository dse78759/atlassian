package atlassian.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atlassian.dao.ContactDAO;
import atlassian.domain.Contact;

/**
 * UNIMPLEMENTED:
 * 
 * Encapsulate any business logic in this layer, so it can be easily tested...
 * 
 * @author scott
 *
 */
@Service
public class ContactService {

	@Autowired
	ContactDAO contactDAO;

	/**
	 * TODO: the logic for save should be:
	 * 		Check if the contact exists already, and, if included, if the associated account exists.
	 * 
	 * 		If the record exists or the account doesn't, direct the controller layer to send back a 400.
	 * 		After save, the post should send back the new id of the record.		
	 * 		
	 */
	int save ( Contact c ) {
		
		return 0;
	}

	/**
	 * TODO: the logic for update should be:
	 * 		Check if the contact exists already
	 * 		If not, direct the controller layer to send back a 404 not found.
	 * 		
	 */
	int update ( Contact c ) {
		
		return 0;
	}

	/** TODO: 
	 * 		If the requested contact is not present, controller should send back a 404.
	 * 
	 * @param id
	 * @return
	 */
	Contact get ( int id ) {
		
		return null;
	}
	
	/** TODO: 
	 * 		If the requested account is not present, controller should send back a 404.
	 * 		If it does, but there are no contacts, send an empty list.
	 * 
	 * @param id
	 * @return
	 */
	List<Contact> getForAccount ( int accountID ) {
		return null;
	}
	
}
